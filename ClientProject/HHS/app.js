App({
  globalData: {
    //全局用户信息--包含用户信息以及设备列表
    userInfo: {},
    deviceNum:'',
    serverAddress:"http://192.168.235.112:8080",
    //请求参数集合---当前状态
    warnList:'/warnList',
    warnImage:'/warnImage',
    warnChartInfo: '/warnChartInfo',
    todayWarnInfo: '/todayWarnInfo',
    //请求参数---模式设置
    setModle: '/setModel'
  },
  tabList: [{
      iconPath: './icon/blueF.png',
      iconSize: "18px",
      iconBottomText: '当前状态',
      jumpingPath: 'pages/current-state/current-state'
    },
    {
      iconPath: './icon/puepleF.png',
      iconSize: "18px",
      iconBottomText: '模式设置',
      jumpingPath: 'pages/modle-setting/modle-setting'
    }
  ],

  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {
  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {

  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {

  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {

  }
})
