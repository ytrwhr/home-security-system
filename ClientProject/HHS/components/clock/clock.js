// components/clock/clock.js
Component({
  options: {
    styleIsolation: 'isolated'
  },
  /**
   * 组件的属性列表
   */
  properties: {  
    clock:{
      type: Object
    }
  },
  clock: function(){
    height:200;
    interval:64;
    strockColor:'';
    clockBgColor:'';
  },
  /**
   * 组件的初始数据
   */
  data: {
    timer:'',
    height: 210,
    bgColor:''
  },
  
  /**
   * 组件的方法列表
   */
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      if(this.data.clock){
        this.setData({
          height: this.data.clock.height == undefined ? 300 : this.data.clock.height,
          bgColor: this.data.clock.clockBgColor == undefined ? '' : this.data.clock.clockBgColor
      })
      };
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  pageLifetimes: {
    show: function() {
      // 页面被展示
      var that = this;
      this.drawClock();
      this.interval=setInterval(function(){
        that.drawClock()
      },1000)  
    },
  },
  methods: {
  /**获得当前时间 */
  getTime: function () {
    let now = new Date()
    let time = []
    time[0] = now.getHours()
    time[1] = now.getMinutes()
    time[2] = now.getSeconds()
    if (time[0] > 12) {
      time[0] -= 12
    }
    return time
  },
  /*初始化时钟的边框*/
  drawClock: function () {
    //一些默认值
    let interval = 64;
    let radius = 32;
    let minuteHand = 100;
    let color = "rgb(19,68,130)";
    if(this.data.clock){
      interval = this.data.clock.interval;
      color = this.data.clock.color;
      minuteHand = this.data.clock.minuteHand;
      radius = this.data.clock.radius;
    }
    //获取canvas
    const query = wx.createSelectorQuery().in(this);
    let canvas = null;
    query.select("#canvas-clock").fields({
      node: true,
      size: true
    }).exec((res) => {
      //通过wxwx.createSelectorQuery() 获取canvas实例
      canvas = res[0].node;
      //通过 canvas.getContext('2d') 获取 CanvasRenderingContext2D 对象
      const ctx = canvas.getContext('2d');
      //获取设备像素比
      const dpr = wx.getSystemInfoSync().pixelRatio;
      canvas.width = res[0].width * dpr;
      canvas.height = res[0].height * dpr;
      //开始绘制边框
      ctx.beginPath();

      //绘制clock 的边框---左上角
      ctx.moveTo(interval + radius, interval);
      ctx.arcTo(interval, interval, interval, interval+radius, radius);

      //绘制clock 的边框---右上角
      ctx.moveTo((canvas.width - (interval + radius)), interval);
      ctx.arcTo((canvas.width - interval), interval, (canvas.width - interval), (interval + radius), radius);

      //绘制clock 的边框---右下角
      ctx.moveTo((canvas.width - interval), (canvas.height - (interval + radius)));
      ctx.arcTo((canvas.width - interval), (canvas.height - interval), (canvas.width - (interval + radius)), (canvas.height - interval), radius);

      //绘制clock 的边框---左下角
      ctx.moveTo((interval), (canvas.height - (interval + radius)));
      ctx.arcTo(interval, (canvas.height - interval), (interval + radius), (canvas.height - interval), radius);
      //绘制刻度表
      //12:00
      ctx.moveTo(canvas.width / 2, interval - 12);
      ctx.lineTo(canvas.width / 2, interval)
      //6:00
      ctx.moveTo(canvas.width / 2, canvas.height - interval);
      ctx.lineTo(canvas.width / 2, canvas.height -  (interval - 12));
      //9:00
      ctx.moveTo( (interval), canvas.height / 2);
      ctx.lineTo( (interval - 12), canvas.height / 2);
      //3:00
      ctx.moveTo(canvas.width -  (interval), canvas.height / 2);
      ctx.lineTo(canvas.width -  (interval - 12), canvas.height / 2);

      //设置时钟边框需要的画笔
      ctx.lineWidth = 16;
      ctx.strokeStyle = color;
      ctx.stroke();
      ctx.closePath();

      //开始绘制指针
      ctx.translate(canvas.width/2,canvas.height/2)
      ctx.rotate(-Math.PI/2)
      /**
       * 获取按当前时间
       */
      let time = this.getTime()
      let h = time[0]
      let m = time[1]
      let s = time[2]
      /**
       * 绘制时钟指针
       */
      ctx.lineWidth = 8;
      ctx.save();
      ctx.rotate(h * Math.PI / 6 + m * Math.PI / 360 + s * Math.PI / 21600)
      ctx.beginPath()
      ctx.moveTo(-10, 0);
      ctx.lineTo(minuteHand/3 * 2, 0)
      ctx.restore();
      ctx.stroke()
      ctx.closePath();

      /**绘制分针 */
       /**
     * 绘制时钟分针
     */
   ctx.save();
    ctx.rotate(m * Math.PI/30 + s * Math.PI/1800)
    ctx.beginPath()
    ctx.moveTo(-10,0)
    ctx.lineTo(minuteHand,0)
    ctx.restore();
    ctx.stroke()
    ctx.restore()

    //绘制秒钟
    ctx.save()
    ctx.rotate(s*Math.PI/30)
    ctx.lineWidth = 6
    ctx.beginPath()
    ctx.moveTo(-10,0)
    ctx.lineTo(minuteHand/3*4,0)
    ctx.stroke()
    });
  },
  }
})
