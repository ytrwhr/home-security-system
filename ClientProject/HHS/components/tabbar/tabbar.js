Component({
  properties:{
    tablist:{
      type:Array,
      value:[]
    },
    fontaftercolor:{
      type: String,
      value: ''
    },
    fontbeforecolor:{
      type: String,
      value: ''
    }
  },
  data:{
    tabList:[],
    fontColor:'',
    fontAfterColor: '',
    fontBeforeColor: ''
  },
  observers:{
    'tablist':function(val){
      this.setData({tabList: val});
      this.data.tabList.forEach(item => {
        item.selected = false;
      });
      console.log("新增元素：");
      console.log(this.data.tabList);
    },
    'fontaftercolor':function(val){
      this.setData({fontAfterColor: val});
    },
    'fontbeforecolor':function(val){
      this.setData({fontBeforeColor: val});
    }
  },
  lifetimes:{
    attached:function(){
      console.log("Notice: tabbar 组件已经生成");
    },
    detached: function(){
      console.log("Notice: tabbar 组件已经销毁");
    }
  },
  methods:{
    jumpingPage:function(e)
    {
      var that = this;
      this.setData({fontColor:that.data.fontAfterColor});
      var index = e.currentTarget.dataset['index'];
      var jumpingPath = that.data.tabList[index].jumpingPath;
      var selected = 'tabList['+index+'].selected';
      this.setData({[selected]: true});
      wx.redirectTo({
        url: "/"+jumpingPath
      });
    },
    recoverFontColor:function(e){
      var that = this;
      var index = e.currentTarget.dataset['index'];
      var selected = 'tabList['+index+'].selected';
      this.setData({[selected]: false});
    }
  }
})