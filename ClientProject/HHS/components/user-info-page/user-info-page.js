Component({
  properties: {
    title: {
      type: String,
      value: "注册设备"
    },
    url: {
      type: String,
      value: ''
    }
  },
  data: {
    requestPath: '',
    isWaitting: false,
    isShowing: true,
    isWarning: "none",
    phoneIconSrc: "cloud://hss-5gq06mqgd5815d4a.6873-hss-5gq06mqgd5815d4a-1310437627/resource/img/phnoe.png",
    deviceIconSrc: "cloud://hss-5gq06mqgd5815d4a.6873-hss-5gq06mqgd5815d4a-1310437627/resource/img/camera.png",
    phoneNumber: '',
    deviceNumber: '',
    warningInfo: "您输入的信息有误，请修改后重试"
  },
  lifetimes: {
    attached: function () {
      //console.log("加入页面树中");
    }
  },
  observers: {
    'url': function (val) {
      this.setData({
        requestPath: val
      });
    }
  },
  lifetimes: {
    attached: function () {
    }
  },
  methods: {
    getPhoneNumer: function (e) {
      //检测输入的数据是否合规
      console.log(e.detail.value);
      this.setData({
        phoneNumber: e.detail.value
      });
    },
    getDeviceNumber: function (e) {
      //检测输入的数据是否合规
      console.log(e.detail.value);
      this.setData({
        deviceNumber: e.detail.value
      });
    },
    registering: function () {
      console.log("注册用户信息ing");
      var phoneNum = this.data.phoneNumber;
      var deviceNum = this.data.deviceNumber;
      var that = this;
      if (phoneNum === undefined || deviceNum === undefined) {
        console.log("数据有误!");
        return;
      }
      //显示等待页
      this.setData({
        isWaitting: true
      });
      //向后台注册用户信息---request
      console.log("注册请求");
      wx.request({
        url: this.data.requestPath,
        data: {
          userInfo:{
            phoneNum: this.data.phoneNumber,
            deviceNum: this.data.deviceNum
          }
        },
        timeout: 3000,
        method: 'POST',
        success: (res) => {
          console.log(res);
          that.setData({ isWaitting: false });
          console.log(res.statusCode.toString()[0]);
          if(res.statusCode.toString()[0] !== '2' ){
            that.setData({
              warningInfo: "服务器繁忙，请重试",
              isWarning: true
            })
          }else{
            that.setData({ isWarning: false }),
            this.setStorage();
          }
        },
        fail: (res) => {
          that.setData({
            isWaitting: false
          });
          that.setData({
            warningInfo: "服务器繁忙，请重试",
            isWarning: true
          })
          console.log(res);
        }
      })
    },
    setStorage: function () {
      //注册成功后，存入本地缓存
      wx.setStorage({
        key: "userInfo",
        data: {
          'phoneNum': this.data.phoneNumber,
          'deviceNum': this.data.deviceNumber
        },
        encrypt: true,
        success: (res)=> {
          console.log(res);
          this.cancleRegister();
        },
        fail:res=>{
          console.log(res);

        }
      });
    },
    cancleRegister: function () {
      console.log("隐藏该弹框")
      setTimeout(function(){},2000);
      this.setData({
        isShowing: false
      });
    }
  }
})