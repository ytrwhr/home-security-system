// pages/current-state/current-state.js
//获取全局变量
import * as echarts from '../../components/ec-canvas/echarts'
var app = getApp();
let warnChart;

function initDate() {
  var timestamp = Date.parse(new Date());
  var date = new Date(timestamp);
  //获取年份  
  var Y = date.getFullYear();
  //获取月份  
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
  //获取当日日期 
  var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

  return {
    year: Y,
    month: M,
    day: D
  }
}

function initChart(canvas, width, height) {
  warnChart = echarts.init(canvas, null, {
    width: width,
    height: height
  });
  canvas.setChart(warnChart);
  var option = {
    title: {
      text: '测试下面legend的红色区域不应被裁剪',
      left: 'center'
    },
    legend: {
      data: ['A', 'B', 'C'],
      top: 20,
      left: 'center',
      // backgroundColor: 'red',
      z: 50
    },
    grid: {
      containLabel: true
    },
    tooltip: {
      show: true,
      trigger: 'axis'
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
      // show: false
    },
    yAxis: {
      x: 'center',
      type: 'value',
      splitLine: {
        lineStyle: {
          type: 'dashed'
        }
      }
      // show: false
    },
    series: [{
      name: 'A',
      type: 'line',
      smooth: true,
      data: [18, 36, 65, 30, 78, 40, 33]
    }, {
      name: 'B',
      type: 'line',
      smooth: true,
      data: [12, 50, 51, 35, 70, 30, 20]
    }, {
      name: 'C',
      type: 'line',
      smooth: true,
      data: [10, 30, 31, 50, 40, 20, 10]
    }]
  };
  warnChart.setOption(option);
  return warnChart;
}

function getChartOption(list) {
  var option = {
    title: {
      text: '警告信息',
      left: 'center'
    },
    legend: {
      data: [], //变量
      top: 30,
      left: 'center',
      // backgroundColor: 'rgba(0,100,200,254)',
      z: 100
    },
    grid: {
      containLabel: true
    },
    tooltip: {
      show: true,
      trigger: 'axis'
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['04:00', '08:00', '12:00', '16:00', '20:00', '24:00'],
      // show: false
    },
    yAxis: {
      x: 'center',
      type: 'value',
      splitLine: {
        lineStyle: {
          type: 'dashed'
        }
      }
    },
    series: []
  };
  list.forEach(item => {
    option.legend.data.push(item['date']);
    option.series.push({
      name: item['date'],
      type: 'line',
      smooth: true,
      data: item['warnCount']
    })
  });
  return option;
}
const getWarnChartInfo = (url, deviceNum, days) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: url,
      data: {
        "days": days,
        "device_number": deviceNum
      },
      method: 'POST',
      success: (res) => resolve(res),
      fail: (res) => reject(res)
    })
  })
}
/**
 * 获取警告信息列表(当天)
 */
const getWarnList = (url, deviceNum) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: url,
      method: 'POST',
      data: {
        device_number: deviceNum
      },
      success: (res) => {
        resolve(res);
      },
      fail: (res) => {
        reject(res)
      }
    })
  })
}

/**
 * 获取警告信息图片(当天)
 */
const getWarnImage = (url, imagePath, deviceNum) => {
  console.log(url)
  return new Promise((resolve, reject) => {
    wx.request({
      url: url,
      responseType: 'arraybuffer',
      method: 'POST',
      data: {
        imagePath: imagePath,
        device_number: deviceNum
      },
      success: (res) => {
        resolve(res);
      },
      fail: (res) => {
        reject(res)
      }
    })
  })
}
Page({
  /**
   * 页面的初始数据
   */
  data: {
    //chart
    ec: {
      onInit: initChart
    },
    showChart: true,
    timing: '',
    phoneNum: '',
    swiperHeight: 400,
    url: 'cloud://hss-5gq06mqgd5815d4a.6873-hss-5gq06mqgd5815d4a-1310437627/resource/Icon/fullscreen.png',
    iconScreenUrl: ['cloud://hss-5gq06mqgd5815d4a.6873-hss-5gq06mqgd5815d4a-1310437627/resource/Icon/fullscreen.png',
      'cloud://hss-5gq06mqgd5815d4a.6873-hss-5gq06mqgd5815d4a-1310437627/resource/Icon/halfscreen.png'
    ],
    deviceList: [],
    deviceListIndex: 0,
    //关闭图片显示图标
    closeInconPath: 'https://6873-hss-5gq06mqgd5815d4a-1310437627.tcb.qcloud.la/resource/Icon/cabcleB.png?sign=b32fe4d36a665a0865c63edeeb2c674e&t=1650877925',
    //警告列表
    warningList: [],
    //警告图片
    warnImagePath: '',
    //显示图片、时钟
    isShowClock: '',
    showWarnImage: false,
    tabList: app.tabList,
    //初始化日期
    date: initDate(),
    //定时执行
    time:''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //获取设备列表参数
    if (options.userInfo) {
      var userInfo = JSON.parse(options.userInfo);
      //赋值全局变量---删除代码
      app.globalData.userInfo = userInfo;
      app.globalData.deviceNum = userInfo.deviceList[0]
      this.setData({
        phoneNum: userInfo.phoneNumber
      });
      this.setData({
        deviceList: userInfo.deviceList
      });
    } else {
      var userInfo = app.globalData.userInfo;
      this.setData({
        phoneNum: userInfo.phoneNumber
      });
      this.setData({
        deviceList: userInfo.deviceList
      });
    }
    //获取设备警告信息
    var that = this;
    var index = this.data.deviceListIndex;
    var deviceNum = this.data.deviceList[index];
    this.setData({
      time: setInterval(()=>{
        this.copyWarnList(deviceNum);
      },1000 *300)
    })
     this.copyWarnList(deviceNum);
  },
  /**
   * 为warnList赋值
   */
  copyWarnList: function (deviceNum) {
    var url = app.globalData.serverAddress + app.globalData.warnList;
    getWarnList(url, deviceNum).then(res => {
      this.setData({
        warningList: res.data.warnList
      });
      console.log(this.data.warningList)
    })
  },
  /**
   *请求图片流
   */
  getImageStream: function (deviceNum, imagePath) {
    var that = this;
    var url = app.globalData.serverAddress + app.globalData.warnImage;
    getWarnImage(url, imagePath, deviceNum).then(res => {
      console.log(res)
      const buffer = res.data;
      var manager = wx.getFileSystemManager();
      manager.writeFile({
        filePath: wx.env.USER_DATA_PATH + '/term.png',
        data: buffer,
        success: res => {
          wx.saveImageToPhotosAlbum({
            filePath: wx.env.USER_DATA_PATH + '/term.png',
            success: function () {
              that.setData({
                isShowClock: 'none',
                showWarnImage: true,
                warnImagePath: wx.env.USER_DATA_PATH + '/term.png'
              });
            },
            fail: function (err) {
              console.log(err)
            }
          })
        }, fail: err => {
          console.log(err)
        }
     });
      console.log(this.data.warnImagePath)
    })
  },
  /**
   * 点击获取图片信息
   */
  lookWarnImage: function (e) {
    console.log(e.currentTarget.dataset['device']);
    this.getImageStream(e.currentTarget.dataset['device'], e.currentTarget.dataset['imagepath']);
  },
  /**
   * 长按保存图片到相册
   */
  /****长按保存图片 */
  saveImg: function () {
    let that = this
    wx.getSetting({
      success(res) {
        //未授权 先授权 然后保存
        if (!res.authSetting['scope.writePhotosAlbum']) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success(re) {
              that.saveToBlum();
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '保存图片到相册',
            success(res) {
              if (res.confirm) {
                that.saveToBlum();
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      }
    })
  },
  //保存到相册方法
  saveToBlum: function () {
    const fsm = wx.getFileSystemManager();
    const filePath = wx.env.USER_DATA_PATH + '/term.png';
    fsm.writeFile({
      filePath,
      data: this.data.imagePath,
      encoding: 'binary',
      success() {
        console.log("itOK")
      },
      fail() {},
    })
    let imgUrl = filePath;
    wx.getImageInfo({
      src: imgUrl,
      success: function (ret) {
        var path = ret.path;
        wx.saveImageToPhotosAlbum({
          filePath: path,
          success(result) {
            wx.showToast({
              title: '保存成功',
              icon: 'success'
            })
          },
          fail(res) {
            wx.showToast({
              title: '保存失败',
              icon: 'fail'
            })
          }
        })
      }
    })
  },
  /**
   * 关闭图片显示窗口
   */
  closeImageWindow: function () {
    this.setData({
      isShowClock: '',
      showWarnImage: false,
    })
  },
  /**
   * 请求警告信息
   */
  getWarnChartInfos: function () {
    //请求警告信息
    var url = app.globalData.serverAddress + app.globalData.warnChartInfo;
    var deviceNum = this.data.deviceList[this.data.deviceListIndex];
    getWarnChartInfo(url, deviceNum, 3).then(res => {
      if (res.statusCode == '200') {
        var opt = getChartOption(res.data.warnCharInfos);
        warnChart.setOption(opt);
      }
    });
  },
  //更改设备
  changeDevice: function (e) {
    this.setData({
      deviceListIndex: e.detail.value
    });
    app.globalData.deviceNum = this.data.deviceList[e.detail.value];
    //更换设备请求信息
  },
  /**
   * 获取时间
   */
  getDateTime:function(e){
    console.log(e)
    var year = e.detail.value.substring(0,4);
    var month = e.detail.value.substring(5,7)
    var day = e.detail.value.substring(8,10)
    this.setData({
      'date.year': year,
      'date.month': month,
      'date.day': day
    });

  },
  /*
   *滑动swiper
   */
  changeSwiper: function () {
    this.setData({
      showChart: this.data.showChart
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */

  onReady() {
    this.getWarnChartInfos();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    console.log("page hide")
    clearInterval(this.data.time);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})