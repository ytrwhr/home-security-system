// pages/index/index.js
//获得全局变量
var app = getApp();
Page({
  /*页面的初始数据*/
  data: {
    url: app.globalData.serverAddress + "/register",
    isFirstLogin: false,
    isWaitting: false,
    resImgPath: "cloud://hss-5gq06mqgd5815d4a.6873-hss-5gq06mqgd5815d4a-1310437627/resource/vidio/HomeAnne.gif",
    userInfo: {
      phoneNumber: '',
      deviceList: []
    }
  },
  /**生命周期函数--监听页面加载 **/
  onLoad: function (options) {},
  /** 获取当前在线监控设备 **/
  getOnlineDevice: function (e) {
    //获取用户电话号码
    console.log("本地获取用户信息");
    var that = this;
    that.setData({
      isWaitting: true
    });
    wx.getStorage({
      key: "userInfo",
      encrypt: true,
      success: (res) => {
        console.log(res.data);
        that.setData({
          isWaitting: false
        });
        that.setData({
          'userInfo.phoneNumber': res.data.phoneNum
        });
        console.log(that.data.userInfo);
        that.getDeviceList();
      },
      fail: function (res) {
        that.setData({
          isWaitting: false
        });
        console.log('没有用户信息')
        console.log(res);
        that.setData({
          isFirstLogin: true
        })
      }
    })
  },
  /** 请求服务器数据**/
  getDeviceList: function () {
    var that = this;
    var userInfo = this.data.userInfo;
    console.log(userInfo)
    //判断数据的正确性
    if (!userInfo.phoneNumber) {
      //读取数据为空重写
      console.log("您的数据为空请重写");
      that.setData({
        isFirstLogin: true
      });
    } else {
      //向服务器发起请求---返回设备列表
      console.log("发起请求" + userInfo.phoneNumber);
      //request
      var address = app.globalData.serverAddress + "/login";
      wx.request({
        url: address,
        timeout: 2000,
        data: {
          "phoneNumber": userInfo.phoneNumber
        },
        method: 'POST',
        success: (res) => {
          that.setData({
            isWaitting: false
          });
          if (!res.data.devList) {
            wx.showToast({
              title: '没有在线设备',
              icon: 'fail',
              duration: 2000
            });
          } else {
            this.setData({
              'userInfo.deviceList': res.data.devList
            });
            app.globalData.userInfo = userInfo;

            // app.globalData.deviceNum = res.data.devList[0];
            //跳转
            var param = JSON.stringify(userInfo);
            console.log("发起请求" + param);
            wx.redirectTo({
              url: '../current-state/current-state?userInfo=' + param,
            })
            console.log(res);
          }

        },
        fail: function (res) {
          that.setData({
            isWaitting: false
          })
          console.log(res);
          console.log("请求登录失败！");
        }
      });
    }

  },
  /** 页面跳转 **/
  jumping: function () {

  },
  /**生命周期函数--监听页面初次渲染完成   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})