// pages/modle-setting/modle-setting.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sleepStartTime: 0,
    sleepEndTime: 0,
    workStartTime: 0,
    workEndTime: 0,
    //tabbar信息
    tabList: app.tabList
  },
  /**
   * 模式设置
   */
  setModle:function() {
   var url = app.globalData.serverAddress + app.globalData.setModle;
   console.log(app.globalData.deviceNum)
    wx.request({
      url: url,
      method: 'POST',
      data: {
        deviceNumber: app.globalData.deviceNum,
        sleepStartTime: this.data.sleepStartTime,
        sleepEndTime: this.data.sleepEndTime,
        workStartTime: this.data.workStartTime,
        workEndTime: this.data.workEndTime,
      },
      success:function(res) {
        console.log(res);
      },
      fail: function(err) {
        console.log(err);
      }
    })
  },
  /**
   * 睡眠模式开始时间
  */
 sleepModelStartTime: function(e) {
   this.setData({
    sleepStartTime: e.detail.value,
    sleepEndTime: (24 - e.detail.value)
   })
 },
 sleepModelEndTime: function(e) {
  this.setData({
    sleepEndTime: e.detail.value,
   })
 },
 workModelStartTime: function(e) {
  this.setData({
    workStartTime: e.detail.value,
    workEndTime: (24 - e.detail.value)
   })
 },
 workModelEndTime: function(e) {
  this.setData({
    workEndTime: e.detail.value,
   })
 },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})