import imp
import cv2
import time
import configparser
import requests
import threading
import datetime
import numpy as np
import src.detect.detect
from turtle import clear
import src.request.local_request as local_request


#get config
con = configparser.ConfigParser()

#read config
con.read('config.ini',encoding='utf-8')

#get server information
server = dict(con.items('server'))
server_url = server['url'] +':' + server['port']
print("sprintboot_url:" + server_url)

#get device information
device = dict(con.items('device'))['number']
print("device_num:" + device)

#get mjpg-stream
mjpg = dict(con.items('mjpg'))['url']
print("mjpg_Streamer:"+mjpg)

#keep aliving
req_path = server_url + "/alive"
res = local_request.KeepActiveRequest(req_path)
print(res)

#设置在线信息
url = server_url + '/online'
res = local_request.OnlineRequest(device,url)
print(res)

#waitting time
waitting_time = int(dict(con.items("waitting"))['time'])

#max-detection 
def run(image_url,url,device_num,imgStream,start_time,max,i):
    #get detece object
    detect = src.detect.detect.Detect()
    image,data = local_request.downloadImg(image_url)
    #detect face and pedestrian
    _,pedestrianImg,pedestrians = detect.detectPedestrians(image)
    faceImg,faces = detect.detectFace(image)
    #If face is detected or pedestrian is detected, Send information to the front end (5s screenshot)
    if (pedestrians > 0 | faces > 0):
            #sent warning massage
            if i==0:
                #init
                max = pedestrians 
                imgStream = data
                start_time = datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d-%H-%M-%S')
            else:
                max = max if max>pedestrians else pedestrians
                imgStream = data
            i += 1
    else:
        if i >= waitting_time:
        # if(i > waitting_time):
            imgStream = data
            start_time = datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d-%H-%M-%S')
            local_request.SentWarningInfo(imgStream,url,device_num,start_time,max)
            i = 0
            max = 0

# pro = threading.Timer(10.0,run(image_url,url,device_num,imgStream,start_time,max,i))
# pro.start()
#warnInfo

#
def modelrun():
    #人体检测
    url = server_url + "/setRaspiModel"
    res = local_request.setWorkModel(url,device)
    
    if(res):
        res = res['setting']
        print(res)

        sST = res['sleepStartTime']
        sET = res['sleepEndTime']
        wST = res['workStartTime']
        wET = res['workEndTime']
        print(sST)
        start_time = datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d-%H-%M-%S')
        hour = start_time[11:13]
        if(sST < sET):
            if hour >= sST and hour <= sET:
                print("现在是睡眠时间")
        else:
            if(hour >= sST or hour <= sET):
                print("现在是睡眠时间")
        if(wST < wET):
            if hour >= wST and hour <= wET:
                print("现在是工作时间")
        else:
            if(hour >= wST or hour <= wET):
                print("现在是工作时间")


warn_path = server_url + "/warning"
while True:
    start_time = ''
    max = ''
    run(mjpg,warn_path,device,start_time,max,0,0)
    modelrun()
    time.sleep(1)
