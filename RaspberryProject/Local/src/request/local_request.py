# -*- coding:utf-8 -*-
import json
import requests
import base64
import datetime
import threading
import numpy as np
import cv2
import src.detect.detect
from urllib import request, response


def downloadImg(url):
    with requests.get(url) as f:
        data = f.content
        img1 = np.frombuffer(data,np.uint8)
        img_cv = cv2.imdecode(img1,cv2.IMREAD_ANYCOLOR)
        return img_cv,data

def KeepActiveRequest(url):
    """keeping alive"""
    print(url)
    header_dict = { "Content-Type": "application/json; charset=utf8" } 
    response = requests.post(url,headers=header_dict)
    return response

def OnlineRequest(dev_number,url):
    """Online request"""
    request_data = { 'deviceNum': dev_number }
    header_dict = { "Content-Type": "application/json; charset=utf8" } 
    response = requests.post(url,headers=header_dict,data = json.dumps(request_data))
    return response


#sent warning information
def SentWarningInfo(imgStreamer,url, deviceNum,start_time,waitting_time):
    #get image content
    imageBytes = base64.encodebytes(imgStreamer)
    img = imageBytes.decode('ascii')
    request_data ={
        'device_number':deviceNum, 
        'image_code': img,
        'max_detection':3,
        'now_time':start_time,
        'standing_time': waitting_time}

    header_dict = { "Content-Type": "application/json; charset=utf8" }
    response = requests.post(url,headers=header_dict,data = json.dumps(request_data))
    return response


#get model
def setWorkModel(url,device_num):
    request_data = {
        'deviceNumber': device_num,
    }
    header_dict = { "Content-Type": "application/json; charset=utf8" }
    response = requests.post(url,headers=header_dict,data = json.dumps(request_data))
    return response.json()
