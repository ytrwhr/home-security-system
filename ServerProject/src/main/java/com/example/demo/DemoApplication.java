package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * @author 20991
 */
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws IOException {
		//设置数据库
		SpringApplication.run(DemoApplication.class, args);
	}

}
