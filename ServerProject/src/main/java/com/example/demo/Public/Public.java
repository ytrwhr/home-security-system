package com.example.demo.Public;

import java.util.List;

public class Public {
    /**
     * 移除参数一中不在参数二中的数据
     * @param dataSource 待去重数据源
     *      * @param list 去重根据
     */
    public static void removeDuplication(List<String> dataSource, List<String> list){
        for(int i = 0;i < dataSource.size(); ++i){
            if(!list.contains(dataSource.get(i))){
                dataSource.remove(i);
            }
        }
    }
}

