package com.example.demo.config;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 20991
 */
@Data
@Component
@ConfigurationProperties(prefix = "config")
public class Config {
    private DataConfig dataConfig;
    public String getFileName(){
        return dataConfig.getFileName();
    }
    public String getSheetName(){
        return dataConfig.getSheetName();
    }
    public String getDeviceFile(){ return dataConfig.getDeviceFile(); }
    public String getDeviceSheet() {
        return dataConfig.getDeviceSheet();
    }
    public String getWarnFileName() { return dataConfig.getWarnFileName();}
    public  String getWarnImageFile() { return dataConfig.getWarnImageFile();}
}
