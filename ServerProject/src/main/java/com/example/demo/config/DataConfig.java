package com.example.demo.config;
import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 20991
 */

@ToString
public class DataConfig {
    private  String fileName;
    private  String sheetName;
    private  String deviceFile;
    private  String deviceSheet;
    private  String warnFileName;
    private String warnImageFile;
    public DataConfig() {
    }

    public void setWarnFileName(String warnFileName) {
        this.warnFileName = warnFileName;
    }

    public  void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public  void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public void setDeviceFile(String deviceFile) {
        this.deviceFile = deviceFile;
    }

    public void setDeviceSheet(String deviceSheet) {
        this.deviceSheet = deviceSheet;
    }

    public void setWarnImageFile(String warnImageFile) { this.warnImageFile = warnImageFile; }

    public String getFileName() {
        return fileName;
    }

    public String getSheetName() {
        return sheetName;
    }

    public String getDeviceFile() {
        return deviceFile;
    }

    public String getDeviceSheet() {
        return deviceSheet;
    }

    public String getWarnFileName() { return warnFileName;}

    public String getWarnImageFile() {  return warnImageFile; }
}
