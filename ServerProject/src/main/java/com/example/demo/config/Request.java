package com.example.demo.config;

/**
 * @author 20991
 * request keys
 */
public class Request {
    public class Login{
        public final static String USER_INFO = "userInfo";
        public final static String PHONE_NUMBER = "phoneNumber";
    }
    public class RequestStatus{
        public final  static String NONE_DEVICE = "";
    }

    /**
     * Warning message returned to the front end
     */
    public  class WarnCharInfo{
        public final  static String DAYS = "days";
        public final static String WARN_CHART = "warnCharInfos";
    }
    /**
     * WarnList
     */
    public  class WarnList{
        public final static String WARNLIST = "warnList";
        public  final  static String IMAGEPATH = "imagePath";
    }
    /**
     * raspberry sent warn information
     */
    public class WarnParam{
        public final  static String IMAGE_CODE = "image_code";
        public final  static String Now_Time  = "now_time";
        public final  static String DEVICE_NUMBER = "device_number";
        public final  static String MAX_DETECTION = "max_detection";
        public  final static String STANDING_TIME = "standing_time";
    }

    /**
     * Model information
     */
    public class Model{
        public final static String DEVICENUM = "deviceNumber";
        public final  static String SLEEPSTARTTIME = "sleepStartTime";
        public final  static String SLEEPENDTIME = "sleepEndTime";
        public final  static String WORKSTARTTIME = "workStartTime";
        public final  static String WORKENDTIME = "workEndTime";
    }
}


