package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.Public.Public;
import com.example.demo.config.Config;
import com.example.demo.config.Request;
import com.example.demo.dao.DeviceDao;
import com.example.demo.dao.UserListDao;
import com.example.demo.dao.WarningInfoDao;
import com.example.demo.pojo.Model;
import com.example.demo.pojo.UserInfo;
import com.example.demo.pojo.WarnChartInfo;
import com.example.demo.pojo.WarningInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.io.IOException;
import java.util.LinkedList;

/**
 * @author 20991
 */
@RestController
public class MiniProgram {
    @Autowired
    private Config config;

    /**
     * @param param
     * @return devList
     * @throws IOException
     */
    @PostMapping("/login")
    public JSONObject login(@RequestBody JSONObject param) throws IOException {
        UserListDao userListDao= new UserListDao(config.getFileName(), config.getSheetName());
        DeviceDao deviceDao = new DeviceDao(config.getDeviceFile(),config.getDeviceSheet());
        JSONObject response = new JSONObject();
        if(!param.isEmpty()){
            if(!(param.getString(Request.Login.PHONE_NUMBER).isEmpty())){
               String devNum = param.getString(Request.Login.PHONE_NUMBER);
               List<String> devList = userListDao.selectDevList(devNum);
               if(!devList.isEmpty()){
                   List<String> onlineDevices = deviceDao.getAllOnlineDev();
                   Public.removeDuplication(devList,onlineDevices);
                   if(!onlineDevices.isEmpty()){
                       response.put("devList",onlineDevices);
                   }
                   else{
                       return null;
                   }

               }
               else{
                   return null;
               }
            }
            else{
                return null;
            }

        }
        else{
            return null;
        }
        return response;
    }

    @PostMapping("/register")
    public JSONObject register(@RequestBody JSONObject param) throws IOException {
        JSONObject res = new JSONObject();
        UserListDao userListDao= new UserListDao(config.getFileName(), config.getSheetName());
        DeviceDao deviceDao = new DeviceDao(config.getDeviceFile(),config.getDeviceSheet());
        if(!(param.isEmpty())){
            JSONObject jsonObject = param.getJSONObject(Request.Login.USER_INFO);
            if(!(jsonObject.isEmpty()) ){
                UserInfo userInfo = JSONObject.toJavaObject(jsonObject,UserInfo.class);
               userListDao.insertUserInfo(userInfo);
                List<String> devList = userListDao.selectDevList(userInfo.getPhoneNum());
                List<String> onlineDevices = deviceDao.getAllOnlineDev();
                if(!devList.isEmpty() && !onlineDevices.isEmpty()){
                    Public.removeDuplication(devList,onlineDevices);
                    res.put("devList",devList);
                }
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
        return res;
    }

    @PostMapping("/warnChartInfo")
    public JSONObject getWarnChartInfo(@RequestBody JSONObject param) throws IOException {
        JSONObject ret = new JSONObject();
        int days = param.getIntValue(Request.WarnCharInfo.DAYS);
        String devNum = param.getString(Request.WarnParam.DEVICE_NUMBER);
        String filePath = config.getWarnFileName() + devNum + ".xlsx";
        WarningInfoDao warnChartSheet = new WarningInfoDao(filePath);
        List<WarnChartInfo> warnChartInfos = warnChartSheet.selectWarnChartInfo(days);
        if(warnChartInfos.isEmpty()){
            return null;
        }
        ret.put(Request.WarnCharInfo.WARN_CHART,warnChartInfos);
        return ret;
    }

    @PostMapping("/todayWarnInfo")
    public JSONObject getWarningInfoToday(@RequestBody JSONObject param) throws IOException {
        JSONObject jsonObject = new JSONObject();
        String deviceNum = param.getString(Request.WarnParam.DEVICE_NUMBER);
        String filePath = config.getFileName() + deviceNum + ".xlsx";
        WarningInfoDao warningInfoDao = new WarningInfoDao(filePath);
        List<WarningInfo> list = warningInfoDao.selectAllWarnInfo(6);
        if(list.isEmpty()){
           return null;
        }

        return jsonObject;
    }

    @RequestMapping("/warnList")
    public JSONObject getWarnList(@RequestBody JSONObject param) throws IOException {
        JSONObject ret = new JSONObject();

        String deviceNum = param.getString(Request.WarnParam.DEVICE_NUMBER);
        String filePath = config.getWarnFileName() + deviceNum + ".xlsx";
        WarningInfoDao warningInfoDao = new WarningInfoDao(filePath);

        List<WarningInfo> list = warningInfoDao.selectTodayWarnList(6);
        if(list.isEmpty()){
            return null;
        }
        ret.put(Request.WarnList.WARNLIST,list);
        return ret;
    }

    @RequestMapping("/warnImage")
    public ResponseEntity<byte[]> getWarnImage(@RequestBody JSONObject param) throws IOException {
        String deviceNum = param.getString(Request.WarnParam.DEVICE_NUMBER);
        String filePath = param.getString(Request.WarnList.IMAGEPATH);
        WarningInfoDao warningInfoDao = new WarningInfoDao(filePath);
        byte[] imageStream = warningInfoDao.getImageStream(filePath);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<>(imageStream,headers, HttpStatus.OK);

    }

    @RequestMapping("/setModel")
    public String setModel(@RequestBody JSONObject param){
        if(param.isEmpty()){
            return "fail";
        }
        String ret = new String("ok");
        String key = param.getString(Request.Model.DEVICENUM);
        Model.ModelItem item = new Model.ModelItem(param.getString(Request.Model.SLEEPSTARTTIME),
                param.getString(Request.Model.SLEEPENDTIME),
                param.getString(Request.Model.WORKSTARTTIME),
                param.getString(Request.Model.WORKENDTIME));
        Model.modelMap.put(key,item);
        return ret;
    }
}
