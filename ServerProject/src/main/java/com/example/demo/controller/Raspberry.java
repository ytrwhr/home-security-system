package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.config.Config;
import com.example.demo.config.Request;
import com.example.demo.dao.DeviceDao;
import com.example.demo.dao.WarningInfoDao;
import com.example.demo.pojo.Model;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author 20991
 */
@RestController
public class Raspberry {
    @Autowired
    private Config config;

    @PostMapping("/alive")
    public JSONObject keepAlive(){
        JSONObject object = new JSONObject();
        object.put("status","ok");
        return object;
    }

    @PostMapping("/online")
    public Boolean inline(@RequestBody JSONObject param) throws IOException {
        DeviceDao deviceDao = new DeviceDao(config.getDeviceFile(), config.getDeviceSheet());
        String deviceNum =  param.getString("deviceNum");
        //Changing Online Status
        deviceDao.alterDeviceInfo(deviceNum);
        return true;
    }

    @RequestMapping("/warning")
    public Boolean sentWarning(@RequestBody JSONObject param) throws IOException {
       if(param.isEmpty()){
           return false;
       }
       String imageCode = param.getString(Request.WarnParam.IMAGE_CODE);
       String fileName = param.getString(Request.WarnParam.DEVICE_NUMBER);

       //xlsx 文件
       fileName = config.getWarnFileName() + fileName + ".xlsx";
       WarningInfoDao warningInfoDao = new WarningInfoDao(fileName);

       //store image
        File file = new File(config.getWarnImageFile() + param.getString(Request.WarnParam.DEVICE_NUMBER));
        if(!file.exists()){
            if(!file.mkdirs()){
                return false;
            }
        }
        String imageFile = config.getWarnImageFile() + param.getString(Request.WarnParam.DEVICE_NUMBER) + "/" + param.getString(Request.WarnParam.Now_Time)+".jpg";
        byte[] bytesImage = imageCode.getBytes("ascii");
        FileOutputStream outputStream = new FileOutputStream(imageFile);
        outputStream.write(Base64.decodeBase64(bytesImage));
        outputStream.close();

       //insert warning information
        return warningInfoDao.insertWarn(param.getString(Request.WarnParam.DEVICE_NUMBER),
                imageFile,
                param.getString(Request.WarnParam.Now_Time),
                param.getIntValue(Request.WarnParam.STANDING_TIME));
    }

    @RequestMapping("/setRaspiModel")
    public JSONObject setRaspiModel(@RequestBody JSONObject param){
        JSONObject ret = new JSONObject();
        String key = param.getString(Request.Model.DEVICENUM);
        if(Model.modelMap.containsKey(key)){
            ret.put("setting", Model.modelMap.get(key));
            Model.modelMap.remove(key);
        }
        return ret;
    }
}
