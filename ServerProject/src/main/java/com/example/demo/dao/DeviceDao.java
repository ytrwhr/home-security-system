package com.example.demo.dao;

import com.example.demo.pojo.DeviceInfo;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author 20991
 */
public class DeviceDao {
    private static List<DeviceInfo> deviceList;
    private String fileName;
    private String sheetName;
    public DeviceDao() {
    }

    public DeviceDao(String fileName, String sheetName) throws IOException {
        this.fileName = fileName;
        this.sheetName = sheetName;
        if(deviceList == null){
            readInfoFromLocalXlsx();
        }
    }

    /**
    * init list (open data souerce)
     **/
    private void readInfoFromLocalXlsx() throws IOException {
        deviceList = new LinkedList<>();
        File file = new File(this.fileName);
        if(!file.exists()){
            XSSFWorkbook wb = new XSSFWorkbook();
            OutputStream outputStream = new FileOutputStream(this.fileName);
            XSSFSheet sheet = wb.createSheet(this.sheetName);
            wb.write(outputStream);
            return;
        }
        FileInputStream fsInput = new FileInputStream(file);
        XSSFWorkbook fxb = new XSSFWorkbook(fsInput);
        XSSFSheet sheet = fxb.getSheet(this.sheetName);
        if(sheet == null){ return; }
        Iterator rows = sheet.rowIterator();
        while(rows.hasNext()){
            XSSFRow row = (XSSFRow) rows.next();
            deviceList.add(new DeviceInfo(row.getCell(0).getStringCellValue(),row.getCell(1).getBooleanCellValue()));
        }
        fsInput.close();
    }

    /**
     *write local file(data source)
     */
    private void writeInfoToLocalXlsx(List<DeviceInfo> wroteInfo) throws IOException {
         deviceList = wroteInfo;
         File file = new File(this.fileName);
        FileInputStream fsInput = new FileInputStream(file);
         XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fsInput);
         XSSFSheet sheet = xssfWorkbook.getSheet(this.sheetName);
         for(int i = 0;i < wroteInfo.size(); ++i){
             XSSFRow row = sheet.createRow(i);
             for(int j = 0;j < DeviceInfo.count; ++j){
                 XSSFCell cell = row.createCell(j);
                 if(j == 0){
                     String value = wroteInfo.get(i).getDeviceNum();
                     cell.setCellValue(value);
                 }else{
                     Boolean value = wroteInfo.get(i).getOnline();
                     cell.setCellValue(value);
                 }
             }

         }
         //delete
         for(int i = wroteInfo.size();i < sheet.getLastRowNum();++i){
             XSSFRow row = sheet.getRow(i);
             sheet.removeRow(row);
         }
         FileOutputStream outputStream = new FileOutputStream(file);
         xssfWorkbook.write(outputStream);
         outputStream.flush();
         outputStream.close();
     }

    /**
     * Insert data to local file
     */
    private void insertInfoToLocalXlsx(DeviceInfo deviceInfo) throws IOException {
        File file = new File(this.fileName);
        FileInputStream fsInput = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fsInput);
        XSSFSheet sheet = xssfWorkbook.getSheet(sheetName);
        if(sheet == null){
            sheet = xssfWorkbook.createSheet(this.sheetName);
        }
        XSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
        for(int i = 0; i < DeviceInfo.count; ++i){
            XSSFCell cell = row.createCell(i);
            if(i == 0){
                String value = deviceInfo.getDeviceNum();
                cell.setCellValue(value);
            }else{
                Boolean value = deviceInfo.getOnline();
                cell.setCellValue(value);
            }
        }
        FileOutputStream outputStream = new FileOutputStream(fileName);
        xssfWorkbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    /**
     * get online devices
     */
    public List<String> getAllOnlineDev(){
        List<String> res = new LinkedList<>();
        ListIterator<DeviceInfo> iter = deviceList.listIterator();
        while(iter.hasNext()){
            DeviceInfo deviceInfo = iter.next();
            if(deviceInfo.getOnline()){
                res.add(deviceInfo.getDeviceNum());
            }
        }
        return res;
    }

    /**
     * alert online status in local file
     * @param key
     */
    private Boolean alterInfoToLocalXlsx(String key) throws IOException {
        int i;
        File file = new File(this.fileName);
        if(!file.exists()){
            return false;
        }
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
        XSSFSheet sheet = xssfWorkbook.createSheet(this.sheetName);
        Iterator rows = sheet.rowIterator();
        for(i = 0; i < sheet.getLastRowNum();++i){
            XSSFRow row = sheet.createRow(i);
            if(row.getCell(0).getStringCellValue().equals(key)){
                row.getCell(1).setCellValue(true);
                break;
            }
        }
        if(i == sheet.getLastRowNum()){
            return false;
        }
        FileOutputStream outputStream = new FileOutputStream(file);
        xssfWorkbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
        return true;
    }
    /**
     * create data source
     */
    public void createDeviceList(List<DeviceInfo> list) throws IOException {
        writeInfoToLocalXlsx(list);
    }

    /**
     * Insert deviceInfo to list
     */
    public boolean insertDeviceInfo(DeviceInfo deviceInfo) throws IOException {
        if(!deviceList.contains(deviceInfo)){
            deviceList.add(deviceInfo);
            insertInfoToLocalXlsx(deviceInfo);
            return true;
        }
        return false;
    }

    /**
     * alter online status from list
     * key: deviceNum
     */
    public boolean alterDeviceInfo(String key) throws IOException {
        ListIterator<DeviceInfo> iter = deviceList.listIterator();
        while(iter.hasNext()){
            DeviceInfo deviceInfo = iter.next();
            if(deviceInfo.getDeviceNum().equals(key)){
                if(alterInfoToLocalXlsx(key)){
                    //alter online status
                    deviceInfo.setOnline(true);
                    return true;
                }
            }
        }
        insertInfoToLocalXlsx(new DeviceInfo(key,true));
        return true;
    }

    /**
     * delete DeviceInfo from list
     * @param deviceInfo
     * @return
     */
    public boolean deleteDeviceInfo(DeviceInfo deviceInfo) throws IOException {
       ListIterator<DeviceInfo> iter =  deviceList.listIterator();
       while(iter.hasNext()){
           if(iter.next().equals(deviceInfo)){
               deviceList.remove(deviceInfo);
               writeInfoToLocalXlsx(deviceList);
               return true;
           }
       }
       return false;
    }

    /**
     * clear data source
     */

    public void clear() throws IOException{
        deviceList.clear();
        File file = new File(this.fileName);
        if(file.exists()){
            file.delete();
        }
        writeInfoToLocalXlsx(deviceList);
    }
}
