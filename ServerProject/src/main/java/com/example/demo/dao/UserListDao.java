package com.example.demo.dao;

import com.example.demo.pojo.UserInfo;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author 20991
 * 用户数据实例化列表
 */
public class UserListDao {
    private static List<UserInfo> userList;
    private String fileName;
    private String sheetName;

    public UserListDao() {
    }

    public UserListDao(String fileName, String sheetName) throws IOException {
        this.fileName = fileName;
        this.sheetName = sheetName;
        if(userList == null){
            readInfoFromLocalXlsx();
        }
    }

    public String getFileName() {
        return fileName;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    /**
     *read date from data source(init list)
     **/
    private void readInfoFromLocalXlsx() throws IOException{
        userList = new LinkedList<>();
        File file = new File(this.fileName);
        if(!file.exists()){
            XSSFWorkbook wb = new XSSFWorkbook();
            OutputStream outputStream = new FileOutputStream(this.fileName);
            XSSFSheet sheet = wb.createSheet(this.sheetName);
            wb.write(outputStream);
            return;
        }
        FileInputStream fsInput = new FileInputStream(file);
        XSSFWorkbook fxb = new XSSFWorkbook(fsInput);
        XSSFSheet sheet = fxb.getSheet(this.sheetName);
        if(sheet == null){
            return;
        }
        Iterator rows = sheet.rowIterator();
        while(rows.hasNext()){
            XSSFRow row = (XSSFRow)rows.next();
            userList.add(new UserInfo(row.getCell(0).getStringCellValue(),row.getCell(1).getStringCellValue()));
        }
        fsInput.close();
      }

    /**
     * write local file(data source)
     */
    private void writeInfoToLocalXlsx(List<UserInfo> wroteInfo) throws IOException{
        userList = wroteInfo;
        File file = new File(this.fileName);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet sheet = xssfWorkbook.getSheet(this.sheetName);
        for(int i = 0;i < wroteInfo.size(); ++i){
            XSSFRow row = sheet.createRow(i);
            for(int j = 0; j < UserInfo.count; ++j){
                XSSFCell cell = row.createCell(j);
                String value = (j == 0 ? wroteInfo.get(i).getPhoneNum() : wroteInfo.get(i).getDeviceNum());
                cell.setCellValue(value);
            }
        }
        for(int i = wroteInfo.size();i < sheet.getLastRowNum();++i){
            XSSFRow row = sheet.getRow(i);
            sheet.removeRow(row);
        }
        FileOutputStream outputStream = new FileOutputStream(file);
        xssfWorkbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    /**
     * Insert data to local file
     */
    private void insertUserInfoToLocalXlsx(UserInfo userInfo) throws IOException {
        FileInputStream inputStream = new FileInputStream(fileName);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);

        XSSFSheet sheet =xssfWorkbook.getSheet(sheetName);
        XSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
        for(int i = 0; i < UserInfo.count; ++i){
            String value = i == 0 ? userInfo.getPhoneNum() : userInfo.getDeviceNum();
            XSSFCell cell = row.createCell(i);
            cell.setCellValue(value);
        }
        FileOutputStream outputStream = new FileOutputStream(fileName);
        xssfWorkbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    /**
     *create a list data
     */
    public void createUserInfoList(List<UserInfo> wroteInfo) throws IOException {
        writeInfoToLocalXlsx(wroteInfo);
    }

    /**
     * insert userInfo to List
     */
    public boolean insertUserInfo(UserInfo userInfo) throws IOException {
        if(!userList.contains(userInfo))
        {
            userList.add(userInfo);
            insertUserInfoToLocalXlsx(userInfo);
            return true;
        }
        //当前已经存在该对象
        return false;
    }

    /**
     *  Select UserInfo From List
     */
    public boolean selectUserInfo(UserInfo userInfo){
        ListIterator<UserInfo> iter = userList.listIterator();
        while(iter.hasNext()){
            UserInfo user = iter.next();
            if(user.equals(userInfo)){
                return true;
            }
        }
        return false;
    }

    /**
     * select devNum from List
     */
    public List<String> selectDevList(String phoneNum){
        List<String> res = new LinkedList<>();
        ListIterator<UserInfo> iter = userList.listIterator();
        while(iter.hasNext()){
            UserInfo userInfo = iter.next();
            if(res.isEmpty() || (userInfo.getPhoneNum().equals(phoneNum) && !(res.contains(userInfo.getDeviceNum())))){
                 res.add(userInfo.getDeviceNum());
            }
        }
        return res;
    }

    /**
     * delete userInfo from list
     */
    public boolean deletedUserInfo(UserInfo userInfo) throws IOException {
        ListIterator<UserInfo> iter = userList.listIterator();
        while(iter.hasNext()){
            UserInfo user = iter.next();
            if(user.equals(userInfo)){
                userList.remove(userInfo);
                writeInfoToLocalXlsx(userList);
                return true;
            }
        }
        return false;
    }

    /**
     * clear data source
     */
    public void clear() throws IOException {
        userList.clear();
        File file = new File(fileName);
        if(file.exists()){
            file.delete();
        }
    }
}
