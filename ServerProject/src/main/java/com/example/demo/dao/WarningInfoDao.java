package com.example.demo.dao;
import com.example.demo.pojo.WarningInfo;
import com.example.demo.pojo.WarnChartInfo;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
/**
 * @author 20991
 */
public class WarningInfoDao {
    private String fileName;
    private String today;

    public WarningInfoDao(String fileName) throws IOException {
        this.fileName = fileName;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
        today = dateFormat.format(new Date()).substring(0, 10);
        openfile();
    }

    /**
     * open file
     */
    private void openfile() throws IOException {
        //创建 xlsx
        File file = new File(this.fileName);
        if(!file.exists()){
            XSSFWorkbook wb = new XSSFWorkbook();
            OutputStream outputStream = new FileOutputStream(this.fileName);
            XSSFSheet sheet = wb.createSheet();
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();
        }
    }

    /**
     * write file
     */
    private Boolean writeInfo(String device_number,String image_path,String now_time,int standingTime) throws IOException {
        File file = new File(this.fileName);
        if(!file.exists()){
            return false;
        }
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet sheet = xssfWorkbook.getSheetAt(0);
        XSSFRow row = sheet.createRow((sheet.getLastRowNum() == -1 ? 0 : sheet.getLastRowNum() + 1));
        row.createCell(0).setCellValue(device_number);
        row.createCell(1).setCellValue(image_path);
        row.createCell(2).setCellValue(now_time);
        row.createCell(3).setCellValue(standingTime);

        FileOutputStream outputStream = new FileOutputStream(file);
        xssfWorkbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
        return true;
    }

    /**
     * select (all info):test
     * Too large an array causes removal
     */
    public List<WarningInfo> selectAllWarnInfo(int minutesInterval) throws IOException {
        List<WarningInfo> ret = new LinkedList<>();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        File file = new File(this.fileName);
        FileInputStream fileInputStream = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
        for(int i =  xssfSheet.getLastRowNum() - 1;i >= 0; --i){
            if(i > 50){
                XSSFRow row = xssfSheet.getRow(i);
                ret.add(new WarningInfo(row.getCell(0).getStringCellValue(),
                        row.getCell(1).getStringCellValue(),
                        row.getCell(2).getStringCellValue(),
                        (int) row.getCell(3).getNumericCellValue()));
            }
        }
        fileInputStream.close();
        return ret;

    }

    /**
     *
     */
    private boolean isRightWarnInfo(int targetMin,int targetHour,int sourceMin,int sourceHour,int intervalMin){
        boolean ret = false;
        int seconds = 60;
        int hours = 24;
        if((targetMin-intervalMin) < 0){
            int rightRage =seconds +(targetMin-intervalMin);
            if(sourceMin >= rightRage || sourceMin <= targetMin){
                if(targetHour == (sourceHour + 1) % 24){
                    ret = true;
                }
            }
        }
        else{
            if(sourceMin <= targetMin && sourceMin > (targetMin - intervalMin) && targetHour == sourceHour){
                ret = true;
            }

        }
        return ret;
    }

    /**
     * select today warnList (Six minutes ahead)
     */
    public List<WarningInfo> selectTodayWarnList(int minutesInterval) throws IOException {
        List<WarningInfo> ret = new LinkedList<>();
        //Gets the current number of minutes
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
        String time = dateFormat.format(new Date());
        int minute = Integer.parseInt(dateFormat.format(new Date()).substring(14,16));
        int hour = Integer.parseInt(dateFormat.format(new Date()).substring(11,13));
        String year = dateFormat.format(new Date()).substring(0,10);
        //test
        minute = 11;
        hour = 22;
        year = "2022-04-21";
        File file = new File(this.fileName);
        if(!file.exists()){
            return null;
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
        System.out.println(xssfSheet.getLastRowNum());
        for(int i = xssfSheet.getLastRowNum();i >= 0; --i){
            XSSFRow row = xssfSheet.getRow(i);
            //get date in warn list
            String yearTrem = row.getCell(2).toString().substring(0,10);
            int minuteTerm = Integer.parseInt(row.getCell(2).toString().substring(14,16));
            int hourTerm = Integer.parseInt(row.getCell(2).toString().substring(11,13));
            if(yearTrem.equals(year))
            {
                if(isRightWarnInfo(minute,hour,minuteTerm,hourTerm,minutesInterval)){
                    ret.add(new WarningInfo(row.getCell(0).getStringCellValue(),
                            row.getCell(1).getStringCellValue(),
                            row.getCell(2).getStringCellValue(),
                            (int) row.getCell(3).getNumericCellValue()));
                }
            }
            else{
                break;
            }
        }
        fileInputStream.close();
        return ret;
    }

    /**
     * Transfer photos
     */
    public byte[] getImageStream(String imageName) throws FileNotFoundException {
        byte[] imageBytes = new byte[1024 * 24];
        File file = new File(imageName);
        if(!file.exists()){
            return  imageBytes;
        }
        InputStream inputStream = new FileInputStream(file);
        //begin read image stream
        int b;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            while((b = inputStream.read(imageBytes)) != -1){

                byteArrayOutputStream.write(imageBytes,0,b);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retrieves information three days prior to the specified date
     * @param days
     * @return
     */
    public List<WarnChartInfo> selectWarnChartInfo(int days) throws IOException {
        //get the chart dot number
        int warnChartInfoArrLen = WarnChartInfo.getLengh();
        int[] warnChartInfoArr = new int[warnChartInfoArrLen];
        String date = today;
        List<WarnChartInfo> ret = new LinkedList<>();

        //open file
        File file = new File(this.fileName);
        FileInputStream fileInputStream = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
        for(int i =  xssfSheet.getLastRowNum();i >= 0; --i){
            XSSFRow row = xssfSheet.getRow(i);
            if(days == 0){
                break;
            }
            if(row.getCell(2).getStringCellValue().substring(0,10).equals(date) &&  i > 0){
                int index = Integer.parseInt(row.getCell(2).getStringCellValue().substring(11,13))%warnChartInfoArrLen;
                warnChartInfoArr[index] += row.getCell(3).getNumericCellValue();
            }
            else{
                ret.add(new WarnChartInfo(warnChartInfoArr,date));
                days--;
                warnChartInfoArr = new int[warnChartInfoArrLen];
                date = row.getCell(2).getStringCellValue().substring(0,10);
            }
        }
        fileInputStream.close();
        return ret;
    }

    /**
     * @param time  the time to select warn information
     * @return
     * @throws IOException
     */
    public List<WarnChartInfo> selectTodayWarnInfo(String time,int days) throws IOException {
        //get the chart dot number
        int warnChartInfoArrLen = WarnChartInfo.getLengh();
        int[] warnChartInfoArr = new int[warnChartInfoArrLen];
        String date = time;
        List<WarnChartInfo> ret = new LinkedList<>();

        //open file
        File file = new File(this.fileName);
        FileInputStream fileInputStream = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
        for(int i =  xssfSheet.getLastRowNum() - 1;i >= 0; --i){
            XSSFRow row = xssfSheet.getRow(i);
            if(days == 0){
                break;
            }
            if(row.getCell(2).getStringCellValue().substring(0,10).equals(date)){
                int index = Integer.parseInt(row.getCell(2).getStringCellValue().substring(11,13))%warnChartInfoArrLen;
                warnChartInfoArr[index] += Integer.parseInt(row.getCell(3).getStringCellValue());
            }
            else{
                ret.add(new WarnChartInfo(warnChartInfoArr,date));
                days--;
                warnChartInfoArr = new int[warnChartInfoArrLen];
                date = row.getCell(2).getStringCellValue().substring(0,10);
            }
        }
        fileInputStream.close();
        return ret;
    }

    /**
     * insert
     */
    public Boolean insertWarn(String device_number,String image_code,String now_time,int standingTime) throws IOException {
        return writeInfo(device_number,image_code,now_time,standingTime);
    }

    /**
     * delect
     */
    public Boolean deleteAllWarnInfo() throws IOException {
        File file = new File(this.fileName);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
        xssfWorkbook.removePrintArea(0);
        return true;
    }
}
