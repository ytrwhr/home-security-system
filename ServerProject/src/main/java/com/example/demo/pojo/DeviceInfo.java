package com.example.demo.pojo;

public class DeviceInfo {
    private String deviceNum;
    private Boolean isOnline;
    public static int count = 2;

    public DeviceInfo() {
        this.isOnline = true;
    }

    public DeviceInfo(String deviceNum, Boolean isOnline) {
        this.deviceNum = deviceNum;
        this.isOnline = isOnline;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public String getDeviceNum() {
        return deviceNum;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public void setDeviceNum(String deviceNum) {
        this.deviceNum = deviceNum;
    }
}
