package com.example.demo.pojo;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author 20991
 */
public class Model {
    public static class ModelItem{
        private String sleepStartTime;
        private String sleepEndTime;
        private String workStartTime;
        private String workEndTime;

        public ModelItem(String sleepStartTime, String sleepEndTime, String workStartTime, String workEndTime) {
            this.sleepStartTime = sleepStartTime;
            this.sleepEndTime = sleepEndTime;
            this.workStartTime = workStartTime;
            this.workEndTime = workEndTime;
        }

        public String getSleepStartTime() {
            return sleepStartTime;
        }

        public String getSleepEndTime() {
            return sleepEndTime;
        }

        public String getWorkStartTime() {
            return workStartTime;
        }

        public String getWorkEndTime() {
            return workEndTime;
        }

        public void setSleepStartTime(String sleepStartTime) {
            this.sleepStartTime = sleepStartTime;
        }

        public void setSleepEndTime(String sleepEndTime) {
            this.sleepEndTime = sleepEndTime;
        }

        public void setWorkStartTime(String workStartTime) {
            this.workStartTime = workStartTime;
        }

        public void setWorkEndTime(String workEndTime) {
            this.workEndTime = workEndTime;
        }
    }
    public static Map<String,ModelItem> modelMap = new Hashtable<String,ModelItem>();
}