package com.example.demo.pojo;

import java.util.Objects;

/**
 * @author 20991
 */
public class UserInfo {
    private String phoneNum;
    private String deviceNum;
    public static int count = 2;

    public UserInfo() {
    }

    public UserInfo(String phoneNum, String deviceNum) {
        this.phoneNum = phoneNum;
        this.deviceNum = deviceNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public void setDeviceNum(String deviceNum) {
        this.deviceNum = deviceNum;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public String getDeviceNum() {
        return deviceNum;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        UserInfo.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (!(o instanceof UserInfo)) {return false;}
        UserInfo userInfo = (UserInfo) o;
        return Objects.equals(phoneNum, userInfo.phoneNum) && Objects.equals(deviceNum, userInfo.deviceNum);
    }
}
