package com.example.demo.pojo;

public class WarnChartInfo {
    private static int lengh = 6;
    private int[] warnCount;
    private String date;

    public WarnChartInfo(int[] warnCount, String date) {
        //this.warnCount = new int[WarnChartInfo.getLengh()];
        this.warnCount = warnCount;
        this.date = date;
    }

    public int[] getWarnCount() {
        return warnCount;
    }

    public String getDate() {
        return date;
    }

    public static int getLengh() {
        return lengh;
    }

    public static void setLengh(int lengh) {
        WarnChartInfo.lengh = lengh;
    }
}
