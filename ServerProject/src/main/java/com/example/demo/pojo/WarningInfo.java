package com.example.demo.pojo;

import java.util.Date;

/**
 * @author 20991
 */
public class WarningInfo {
    String deviceNum;
    String imagePath;
    String date;
    int standingTime;

    public WarningInfo() {
    }

    public WarningInfo(String deviceNum,  String imagePath, String date,int standingTime) {
        this.deviceNum = deviceNum;
        this.imagePath = imagePath;
        this.date = date;
        this.standingTime = standingTime;
    }

    public String getDeviceNum() {
        return deviceNum;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getDate() {
        return date;
    }

    public int getStandingTime() {
        return standingTime;
    }
}
